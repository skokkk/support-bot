<?php

$cid = 6418; 				// Channel ID
$name = "support-bot"; 		// Query login name & bot name
$pw = "iloveskokkk";		// Query password
$addr = "localhost"; 		// Address to connect to
$qport = 10011;				// Query port for your server

// load framework files
require_once("libraries/TeamSpeak3/TeamSpeak3.php");

// connect to local server in non-blocking mode, authenticate and spawn an object for the virtual server on port 9987
$ts3_VirtualServer = TeamSpeak3::factory("serverquery://".$name.":".$pw."@".$addr":".$qport."/?server_port=9987&blocking=0&nickname=".$name);

// get notified on incoming private messages
$ts3_VirtualServer->notifyRegister("textprivate");

// register a callback for notifyTextmessage events 
TeamSpeak3_Helper_Signal::getInstance()->subscribe("notifyTextmessage", "onTextmessage");

// wait for events
while(1) $ts3_VirtualServer->getAdapter()->wait();

// define a callback function
function onTextmessage(TeamSpeak3_Adapter_ServerQuery_Event $event, TeamSpeak3_Node_Host $host)
{
global $cid;
global $ts3_VirtualServer;
 echo "Client " . $event["invokername"] . " sent textmessage: " . $event["msg"] . "\n";

	switch ($event["msg"]) {
		case "!open":
		    echo "Open Sesame!\n";
			$ts3_VirtualServer->channelGetById($cid)->message("Open Sesame!");  
			//$ts3_VirtualServer->channelGetById($cid)->modify("name", "Director");  
			$channel = $ts3_VirtualServer->channelGetById($cid);
			$channel['channel_name'] = "Support Lounge [Open]";
			$ts3_VirtualServer->channelPermRemove($cid, "i_channel_needed_join_power");
			$ts3_VirtualServer->channelPermRemove($cid, "i_channel_needed_subscribe_power");
		    break;
		case "!close":
		    echo "Close Sesame!\n";
			$ts3_VirtualServer->channelGetById($cid)->message("Close Sesame!");  
			$channel = $ts3_VirtualServer->channelGetById($cid);
			$channel['channel_name'] = "Support Lounge [Closed]";
			$ts3_VirtualServer->channelPermAssign($cid, "i_channel_needed_join_power", 100);

		    break;
		case "!hide":
			echo "Hidden Sesame!\n";
			$ts3_VirtualServer->channelGetById($cid)->message("Hidden Sesame!");  
			$channel = $ts3_VirtualServer->channelGetById($cid);
			$channel['channel_name'] = "Support Lounge [Busy]";
			$ts3_VirtualServer->channelPermAssign($cid, "i_channel_needed_join_power", 100);
			$ts3_VirtualServer->channelPermAssign($cid, "i_channel_needed_subscribe_power", 100);
		    break;
		default:
		    echo "Error.\n";
	}

}

?>
